// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  app: {
    baseURL: '/kbcc-strapi'
  },
  css: [
    '~/assets/css/common_old.css',
    '~/assets/css/swiper.min.css',
    '~/assets/css/sync.css',
    '~/assets/css/morpheus-style.css',
    // '~/assets/css/morpheus-reset.min.css',
    // '~/assets/css/font.css'
  ],
  runtimeConfig: {
    public: {
        baseUrl: 'http://125.131.88.58:1337',
        apiBaseUrl: 'http://125.131.88.58:1337/api'
    }
  },
  plugins: [
    
  ],
  dir: {
    public: 'realpublic'
  }
})
