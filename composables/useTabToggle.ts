export default (tabs: Array<any>) => {
  const currentTab = ref('');

  const toggleEvent = (selectedItem: { name : string, selected : boolean }) => {
    tabs.forEach((item) => {
      if (item.name !== selectedItem.name) {
        item.selected = false;
      }
    });
  
    selectedItem.selected = !selectedItem.selected;
    currentTab.value = selectedItem.name;
  }

  return {
    currentTab,
    toggleEvent
  }
}