export default (name: string) => {
  const runtimeConfig = useRuntimeConfig();

  return useFetch(name, {
    baseURL: runtimeConfig.public.apiBaseUrl,
    onRequest({ request, options }) {
      // console.log(`[useApiReq] : API 요청`);
      // console.log(`[useApiReq] : ${request}`);
    },
    onRequestError({ request, options, error }) {
       // console.log(`[useApiReq] : 요청중 오류 발생`);
       // console.log(error);
    },
    onResponse({ request, response, options }) {
       // console.log(`[useApiReq] : 응답 완료`);
       // console.log(`[useApiReq] : ${response.status}`);
    },
    onResponseError({ request, response, options }) {
       // console.log(`[useApiReq] : 응답중 오류 발생`);
       // console.log(`[useApiReq] : ${response.status}`);
    }
  })
}